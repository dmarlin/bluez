#!/bin/bash

set -ex

# This test is designed such that a Bluetooth controller is not required but we
# can at least check to make sure the commands are installed correctly

# Is bccmd present and doesn't fail if called
#bccmd --help

# Is bluemoon present and doesn't fail if called
bluemoon --help

# Is bluetoothctl present and doesn't fail if called
bluetoothctl --help

# Is btattach present and doesn't fail if called
btattach --version

# Is btmgmt present and doesn't fail if called
btmgmt --help

# Is btmon present and doesn't fail if called
btmon --help

# Is hex2hcd present and doesn't fail if called
hex2hcd --help

# Is mpris-proxy present and doesn't fail if called
mpris-proxy --help

